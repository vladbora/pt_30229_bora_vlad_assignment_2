package simulation;

import simulation.Scheduler;
import task.*;

import java.io.*;
import java.util.*;

public class SimulationManager extends Thread {

    private List<Client> clienti = new ArrayList<Client>();
    private int queueNumber;
    private int totalClients;
    private int simulationInterval;
    private int minArrivingTime;
    private int maxArrivingTime;
    private int minServiceTime;
    private int maxServiceTime;
    private int simTime = 0;
    private boolean toRun = true;
    private Scheduler scheduler = new Scheduler();
    private String outputName;

    // un constructor care populeaza campurile, genereaza clienti si creaza servere
    public SimulationManager(int totalClient, int queueNumber, int simulationInterval, int minArrivingTime, int maxArrivingTime,
                             int minServiceTime, int maxServiceTime, String outPutName) {
        this.totalClients = totalClient;
        this.queueNumber = queueNumber;
        this.simulationInterval = simulationInterval;
        this.minArrivingTime = minArrivingTime;
        this.maxArrivingTime = maxArrivingTime;
        this.minServiceTime = minServiceTime;
        this.maxServiceTime = maxServiceTime;
        this.outputName = outPutName;

        for (int index = 0; index < totalClient; ++index){
            clienti.add(generateRandomClient(index + 1));
        }

        for (int index = 0; index < queueNumber; ++index) {
            scheduler.addServer(new Server());
        }
    }

    // este o metoda care genereaza un nr random intr-un interval
    private int generateNumberInInterval(int lowerBound,int upperBound){
        Random rand = new Random();
        int number = rand.nextInt(upperBound - lowerBound + 1);
        number += lowerBound;
        return number;
    }

    // genereaza un client random
    public Client generateRandomClient(int id){
        Client cl = new Client();
        cl.setId(id);
        cl.setArrivalTime(generateNumberInInterval(this.minArrivingTime, this.maxArrivingTime));
        cl.setServiceTime(generateNumberInInterval(this.minServiceTime, this.maxServiceTime));
        return  cl;
    }

    // run-ul se ocupa de trecerea timpului si de operatiile care apar
    @Override
    public synchronized void run() {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(outputName, "UTF-8");
        }
        catch (FileNotFoundException e) {
        }
        catch (UnsupportedEncodingException e) {

        }
        float finalWaitingTime = 0;
        startAllServers();
        while(toRun) {
            checkAddClient();
            printCurrentState(writer);
            finalWaitingTime += scheduler.getCurrentWaitingTime();
            simTime++;
            for (Server sv : scheduler.getServers()){
                sv.setTimePassed(true);
                sv.interrupt();
            }
            try {
                sleep(10);
            }
            catch (InterruptedException e) {
            }
            if (!scheduler.areThereClients() && clienti.isEmpty()){
                toRun = false;
                break;
            }
            if (simTime == simulationInterval)
                toRun = false;
        }
        writer.println("Average waiting time: " + finalWaitingTime / totalClients);
        for (Server ser : scheduler.getServers()){
            ser.setToRun(false);
        }
        writer.close();
        for (Server ser : scheduler.getServers()){
            ser.interrupt();
        }
    }

    // o metoda care verifica daca poate sa adauge clienti in servere
    private void checkAddClient(){
        Iterator<Client> var = clienti.iterator();
        while(var.hasNext()) {
            Client aux = var.next();
            if (aux.getArrivalTime() <= simTime) {
                scheduler.getSmallestQueue().addClient(aux);
                var.remove();
            }
        }
    }

    // o metoda care porneste toate serverele
    private void startAllServers(){
        int i = 1;
        for (Server server : scheduler.getServers()) {
            server.setName("Queue " + i);
            server.start();
            i++;
        }
    }

    // o metoda care ne afiseaza starea curenta a clientilor si a serverelor
    private void printCurrentState(PrintWriter writer){
        writer.println("Time " + simTime);
        writer.print("Waiting clients: ");
        for (Client cl : clienti) {
            writer.print(cl + ";");
        }
        writer.println();
        for (Server ser : scheduler.getServers()) {
            writer.println(ser);
        }
    }
}
