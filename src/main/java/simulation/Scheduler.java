package simulation;

import java.io.*;
import java.util.*;
import task.*;

public class Scheduler{


    private List<Server> queues = new ArrayList<Server>();

    public void Scheduler(){

    }

    public void addServer(Server sv){
        queues.add(sv);
    }

    public List<Server> getServers(){
        return queues;
    }

    // aceasta metoda ne gaseste serverul cel mai potrivit
    // folosing o strategie de tip gready
    public Server getSmallestQueue() {
        Server minSer = queues.get(0);
        for (Server ser : queues){
            if (ser.getWaitingPeriod() < minSer.getWaitingPeriod()){
                minSer = ser;
            }
        }
        return minSer;
    }

    // ne spune daca avem clienti in servere
    public boolean areThereClients(){
        for (Server sv : queues){
            if (sv.hasClients())
                return  true;
        }
        return false;
    }

    // ne spune cat au asteptati clientii la coada pentru o iteratie
    public int getCurrentWaitingTime(){
        int sum = 0;
        for (Server sv : queues){
            sum += sv.getCurrentWaitingTime();
        }
        return sum;
    }

}

