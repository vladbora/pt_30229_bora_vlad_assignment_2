package main;

import simulation.SimulationManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MainClass {

    public static void main(String[] args)
    {
        String filename = args[0];
        int totalClients;
        int totalQueues;
        int simulationInterval;
        int minArrivingTime;
        int maxArrivingTime;
        int minServiceTime;
        int maxServiceTime;

        String data = new String();
        // deschidem fisierul
        try{
            File f = new File(filename);
            Scanner sc = new Scanner(f);
            while(sc.hasNextLine()){
                data = data + " " + sc.nextLine();
            }
        } catch (FileNotFoundException e) {
            System.out.println("error file not found");
            e.printStackTrace();
        }

        //folosim un regex pentru a face split-ul
        String[] values = data.split("\\D+");

        // facem niste parsari pentru fiecare dintre date
        totalClients =Integer.parseInt(values[1]);
        totalQueues = Integer.parseInt(values[2]);
        simulationInterval= Integer.parseInt(values[3]);
        minArrivingTime= Integer.parseInt(values[4]);
        maxArrivingTime= Integer.parseInt(values[5]);
        minServiceTime= Integer.parseInt(values[6]);
        maxServiceTime= Integer.parseInt(values[7]);

        String outPut = new String();
        outPut = args[1];
        //deschidem threadul principal
        Thread t = new Thread(new SimulationManager(totalClients, totalQueues, simulationInterval, minArrivingTime, maxArrivingTime,
        minServiceTime, maxServiceTime, outPut));
        t.start();
    }
}
