package task;


import task.Client;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Server extends Thread {
    public BlockingQueue<Client> queue = new ArrayBlockingQueue<Client>(100); // aceasta este coada in care ne tinem clienti
    private int waitingPeriod = 0; // folosim aceasta variabila pentru a afla cat timp petrec oameni la coada
    private volatile boolean toRun = true; // este un flag care ne tine in run
    private volatile boolean timePassed = false; // acest flag ne spune daca a trecut un timp

    public Server() {

    }

    public String toString() {
        String s = this.getName() + ": ";
        if (queue.isEmpty())
            return s + "closed";
        for (Client cl : queue){
            s += cl + ";";
        }
        return s;
    }

    public void setToRun(boolean toRun) {
        this.toRun = toRun;
    }

    public int getWaitingPeriod(){
        return  waitingPeriod;
    }

    // o functie de adaugare a unui client
    public void addClient(Client client) {
        queue.add(client);
        waitingPeriod += client.getServiceTime();
    }

    public boolean getToRun(){
        return  toRun;
    }

    // functia run decrementeaza serviceTime pentru primul client
    // dupa care asteapta sa mai treaca o secunda
    @Override
    public synchronized void run() {
        while(toRun) {
            if (timePassed){
                this.decreaseFirstClient();
                timePassed = false;
            }
            else {
                try {
                    wait();
                } catch (InterruptedException e) {
                    if (Thread.currentThread().isInterrupted()) {
                    }
                }
            }
        }
    }

    public void setTimePassed(boolean hasTimePassed){
        this.timePassed = hasTimePassed;
     }

     // aceasta metoda verifica daca exita clienti si daca da
    // decrementeaza serviceTime-ul lor si daca acesta este 0
    // elimina clientul
    public void decreaseFirstClient() {
        if (!queue.isEmpty()){
            queue.peek().decreseServiceTime();
            waitingPeriod--;
            if (queue.peek().getServiceTime() == 0)
                queue.remove();
        }
    }

    // aceasta functie ne spune daca mai avem clienti
    public boolean hasClients(){
        return !queue.isEmpty();
    }

    public int getCurrentWaitingTime(){
        return queue.size();
    }
}