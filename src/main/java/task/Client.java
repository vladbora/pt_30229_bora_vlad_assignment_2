package task;

public class Client {
    private int id; // id reprezinta un identificator unic
    private int arrivalTime; // arrival time reprezinta timpul la cara ajunge un client
    private int serviceTime; // serrvice time este timpul la de aspteptare pentru ca un client sa fie servit

    // aceasta clasa are rolul de task si ne reprezinta in contextul acual clientul
    public Client(){

    }

    public Client(int id, int arrivalTime, int serviceTime) {
        this.id = id;
        this.arrivalTime = arrivalTime;
        this.serviceTime = serviceTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }

    public String toString() {
        String s = "";
        s += "(" + id + "," + arrivalTime + "," + serviceTime + ")";
        return s;
    }

    public void decreseServiceTime() {
        serviceTime--;
    }


}
